---
layout: post
title: Three great programming blogs
categories: ['post']
tags: ['post']
---

## Three great programming blogs

These are some of my favourite programming bloggers. All 3 of these are consistently knocking out quality posts and well worth a follow:

1. [Martin Heinz](https://martinheinz.dev/)
2. [Adam Johnson](https://adamj.eu/tech)
3. [Roman Imankulov](https://roman.pt/)
