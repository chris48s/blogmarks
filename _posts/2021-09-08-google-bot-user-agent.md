---
layout: post
title: Browse The Web Like A Crawler
categories: ['web']
tags: ['web']
---

## Browse The Web Like A Crawler

Some sites sneakily serve different content to crawlers and interactive users.
Setting your user agent to:

```
Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)
```

allows you to see what GoogleBot sees. [Uaswitcher](https://addons.mozilla.org/en-GB/firefox/addon/uaswitcher/) for firefox has a preset for this out of the box.
