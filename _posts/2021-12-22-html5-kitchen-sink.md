---
layout: post
title: HTML 5 Kitchen Sink
categories: ['html']
tags: ['html']
---

## [HTML 5 Kitchen Sink](https://github.com/dbox/html5-kitchen-sink)

[HTML 5 Kitchen Sink](https://github.com/dbox/html5-kitchen-sink) is really useful for testing out themes and stylesheets. It also has the helpful side effect of introducing me to (or reminding me about) some of the less common HTML5 elements that exist in the spec as I use it.
